<?php

namespace Tests\Feature;

use App\Models\Task;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class TaskTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test listing tasks.
     *
     * @return void
     */
    public function test_tasks_can_be_listed()
    {
        Task::factory()->count(10)->create();

        $response = $this->getJson('/api/v1/tasks');

        $response->assertStatus(Response::HTTP_OK)
            ->assertJsonCount(1);
    }

    /**
     * Test creating a task.
     *
     * @return void
     */
    public function test_task_can_be_created()
    {
        $data = [
            'name' => 'New Task',
            'is_completed' => false,
        ];

        $response = $this->postJson('/api/v1/tasks', $data);

        $response->assertStatus(Response::HTTP_CREATED)
            ->assertJsonFragment($data);
    }

    /**
     * Test updating a task.
     *
     * @return void
     */
    public function test_task_can_be_updated()
    {
        $task = Task::factory()->create();

        $data = [
            'name' => 'Updated Task Name',
            'is_completed' => true,
        ];

        $response = $this->patchJson("/api/v1/tasks/{$task->id}", $data);

        $response->assertStatus(Response::HTTP_OK)
            ->assertJsonFragment($data);
    }

    /**
     * Test marking a task as complete.
     *
     * @return void
     */
    public function test_task_can_be_marked_as_complete()
    {
        $task = Task::factory()->create(['is_completed' => false]);

        $data = ['is_completed' => true];

        $response = $this->patchJson("/api/v1/tasks/{$task->id}/complete", $data);

        $response->assertStatus(Response::HTTP_OK)
            ->assertJsonFragment(['is_completed' => true]);
    }

    /**
     * Test deleting a task.
     *
     * @return void
     */
    public function test_task_can_be_deleted()
    {
        $task = Task::factory()->create();

        $response = $this->deleteJson("/api/v1/tasks/{$task->id}");

        $response->assertStatus(Response::HTTP_OK)
            ->assertJson(['message' => 'Task deleted successfully']);
    }
}
